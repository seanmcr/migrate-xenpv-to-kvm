# migrate-xenpv-to-kvm

Scripts to assist migrating from Xen PV to KVM

# license

```
Copyright (C) 2020 Sean McRobbie
contact@seanmcrobbie.co.uk

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation,
Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
```



# purpose

*  Takes an input image in the form of a raw ext3 formatted file which will have a Linux distribution installed on it but no Kernel or Grub
*  Creates a new partitioned disk suitable for booting on KVM
*  Modifies the following distributions to work on KVM:
     - CentOS 5
     - CentOS 6
     - Debian 5
     - Debian 6
     - Ubuntu 10.04
     - Ubuntu 10.10
     - Ubuntu 12.04
     - Ubuntu 12.10

The current version of this script was never designed to be pretty as it was only ever intended for a one-time migration invoked either manually or in the case of OpenITC by other backend systems which perform validation before feeding parameters.

# prereqs

On a CentOS 7 system:

`yum -y install libguestfs-tools libguestfs-xfs`

# how-to

launch `migrate-xenpv-to-kvm.sh` with the following parameters in order:

1.  vmname (used to mount the disk image in /dev/<volgroup>/<vmname>_rootimg)
2.  volgroup
3.  full disk size, in MB
4.  swap size, in MB
5.  0 (disused now, leave at 0)
6.  <distro><version> e.g. centos6, ubuntu12.04

# example

The below example will:

*  Expect a plain ext3 fs at /dev/VolGroup00/vps1234.vm_rootimg
*  Create new partitioned disk of 10240MB at /dev/VolGroup00/vps1234.vm
*  Create a 256MB ext2 /boot partition (see bootsize=256)
*  Create a 1024MB swap partition
*  Create an ext3 / partition with the rest of the disk
*  Migrate the data across according to ubuntu12.04

`./migrate-xenpv-to-kvm.sh vps1234.vm VolGroup00 10240 1024 0 ubuntu12.04`

# post-migration

not included here are other important steps you may need to consider afterwards which we have handled separately using libguestfs-python:

* Inspecting the guest OS
* Mounting it
* Configuring files such as network interfaces for the defined distribution/version

# improvements

It would be nice to re-write this tool in pure python with libguestfs-python as I'm already using that for the backend of OpenITC AutoConf

Contributions and maintainers are welcome
